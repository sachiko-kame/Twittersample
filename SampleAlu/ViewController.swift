//
//  ViewController.swift
//  SampleAlu
//
//  Created by 水野祥子 on 2018/02/08.
//  Copyright © 2018年 sachiko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func share(_ sender: Any) {
        print("シェア")
        if let session = TWTRTwitter.sharedInstance().sessionStore.session() {
            print(session.userID)
            let composer = TWTRComposer()
            composer.setText("Hello Twitter!")
            composer.show(from: self, completion: nil)
        } else {
            print("アカウントはありません")
        }
    }
    
    @IBAction func login(_ sender: Any) {
        print("ログイン(連携)")
        
        TWTRTwitter.sharedInstance().logIn { session, error in
            guard let session = session else {
                if let error = error {
                    print("エラーが起きました => \(error.localizedDescription)")
                }
                return
            }
            print("@\(session.userName)でログインしました")
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        print("ログアウト(連携解除)")
        
        let sessionStore = TWTRTwitter.sharedInstance().sessionStore
        
        // アクティブなアカウントのsessionを取得
        if let session = sessionStore.session() {
            // userIDでログアウト
            sessionStore.logOutUserID(session.userID)
        }
    }
    
    
}

